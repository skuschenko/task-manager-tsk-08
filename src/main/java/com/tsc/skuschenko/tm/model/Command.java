package com.tsc.skuschenko.tm.model;

public class Command {

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

    private String arg = "";

    private String name = "";

    private String description = "";

    public Command(String name, String arg, String description) {
        this.arg = arg;
        this.name = name;
        this.description = description;
    }

    public Command() {
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
