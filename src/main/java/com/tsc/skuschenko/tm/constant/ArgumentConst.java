package com.tsc.skuschenko.tm.constant;

public class ArgumentConst {

    public static final String ARG_VERSION = "-v";

    public static final String ARG_HELP = "-h";

    public static final String ARG_ABOUT = "-a";

    public static final String ARG_INFO = "-i";

}
