package com.tsc.skuschenko.tm.constant;

public class TerminalConst {

    public static final String TM_VERSION = "version";

    public static final String TM_HELP = "help";

    public static final String TM_ABOUT = "about";

    public static final String TM_EXIT = "exit";

    public static final String TM_INFO = "info";

    public static final String TM_COMMANDS = "commands";

    public static final String TM_ARGS = "arguments";

}
