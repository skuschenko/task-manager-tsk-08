package com.tsc.skuschenko.tm;

import com.tsc.skuschenko.tm.api.ICommandRepository;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Command;
import com.tsc.skuschenko.tm.repository.CommandRepository;
import com.tsc.skuschenko.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        System.out.println("***Welcome to task manager***");
        if (parseArgs(args)) exit();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter command:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg.toLowerCase()) {
            case ArgumentConst.ARG_ABOUT:
                showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                showSystemInfo();
                break;
            default:
                showWrongArg(arg);
                break;
        }
    }

    private static void parseCommand(final String command) {
        if (command == null) return;
        switch (command.toLowerCase()) {
            case TerminalConst.TM_ABOUT:
                showAbout();
                break;
            case TerminalConst.TM_VERSION:
                showVersion();
                break;
            case TerminalConst.TM_HELP:
                showHelp();
                break;
            case TerminalConst.TM_EXIT:
                exit();
                break;
            case TerminalConst.TM_INFO:
                showSystemInfo();
                break;
            case TerminalConst.TM_COMMANDS:
                showCommands();
                break;
            case TerminalConst.TM_ARGS:
                showArgs();
                break;
            default:
                showWrongCmd(command);
                break;
        }
    }

    private static void showWrongCmd(String cmd) {
        System.out.print(cmd + " command doesn't  found. ");
        System.out.println("Pleas print '" + TerminalConst.TM_HELP + "' for more information");
    }

    private static void showWrongArg(String arg) {
        System.out.print(arg + " argument doesn't  found. ");
        System.out.println("Pleas print '" + TerminalConst.TM_HELP + "' for more information");
    }

    private static boolean parseArgs(String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void showAbout() {
        System.out.println("[" + TerminalConst.TM_ABOUT.toUpperCase() + "]");
        System.out.println("AUTHOR: Semyon Kuschenko");
        System.out.println("EMAIL: skushchenko@tsconsulting.com");
    }

    private static void showVersion() {
        System.out.println("[" + TerminalConst.TM_VERSION.toUpperCase() + "]");
        System.out.println("1.0.0");
    }

    private static void showHelp() {
        System.out.println("[" + TerminalConst.TM_HELP.toUpperCase() + "]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command);
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showSystemInfo() {
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemotyVal = (isMaxMemory ? "no limit" : NumberUtil.formatSize(maxMemory));
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("[" + TerminalConst.TM_INFO.toUpperCase() + "]");
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + NumberUtil.formatSize(freeMemory));
        System.out.println("Maximum memory: " + maxMemotyVal);
        System.out.println("Total memory available to JVM: " + NumberUtil.formatSize(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.formatSize(usedMemory));
        System.out.println("[OK]");
    }

    private static void showCommands() {
        System.out.println("[" + TerminalConst.TM_COMMANDS.toUpperCase() + "]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    private static void showArgs() {
        System.out.println("[" + TerminalConst.TM_ARGS.toUpperCase() + "]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String arg = command.getArg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }

}